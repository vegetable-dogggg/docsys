"""system URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.views.static import serve
from django.urls import path, re_path
from system import views, settings

urlpatterns = [
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}, name='static'),
    path('', views.welcome),
    path('send/', views.upload),
    path('index/', views.index),
    path('login/', views.login),
    path('register/', views.register),
    path('download/',views.download),
    path('receive/',views.receive),
    path('delete/',views.delete),
    path('admin/',views.admin),
    path('destroy/', views.destroy),
    path('duesr/', views.duesr, name='duesr'),
]

handler404 = views.page_not_found
handler500 = views.error