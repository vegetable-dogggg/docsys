from django.db import models

#用户类
class user(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    password = models.CharField(max_length=64)
    email = models.EmailField(max_length=100)
    level = models.CharField(max_length=10,default="三级单位")
    # def __str__(self):
    #     return "<user object: {}{}{}{}>".format(self.id,self.name,self.password,self.email)

#文件类
class library(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    send_department = models.ForeignKey(to=user ,on_delete=models.deletion.CASCADE,related_name='library_acc_department')
    acc_department = models.ForeignKey(to=user,on_delete=models.deletion.CASCADE,related_name='library_send_department')
    path = models.CharField(max_length=100)
    pac_path = models.CharField(max_length=100)
    is_delete = models.CharField(max_length=64)
    level = models.CharField(max_length=10,default="秘密")
    # def __str__(self):
    #     return "<library object: {}{}{}{}{}{}{}>".format(self.id,self.name,self.send_department,self.acc_department,self.path,self.pac_path,self.time)