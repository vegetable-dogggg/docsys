from django.contrib import messages
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from . import models
from .forms import UploadFileForm
from .upload import handle_uploaded_file
# 将请求定位到index.html文件中

def welcome(request):
    request.session['D_name'] = ''
    request.session['D_id'] = ''
    return render(request, 'welcome.html')

def index(request):        #数据绑定
    if request.session['D_name'] !='':
        print(request.method)
        D_id = request.session.get('D_id')
        # print(type(D_id),"->",D_id)
        files = models.library.objects.filter(send_department=D_id)
        print(files)
        return render(request, 'index.html', {'filelist': files})
    return render(request, 'welcome.html')

def login(request):
    # POST请求表示用户已提交数据
    if request.method == 'POST':
        name = request.POST.get("in_name")
        # print (name)
        password = request.POST.get("in_password")
        # print(password)
        if name and password:
            try:
                User = models.user.objects.get(name=name)
                if User.password == password:
                    request.session['D_id'] = User.id
                    request.session['D_name'] = User.name
                    if (User.name == 'root'):
                        return redirect('/admin/')
                    else:
                        return redirect('/index/')
                # print (User.id)
            except:
                messages.info(request, "账号或密码错误！")
                return redirect('/login/')
        #
        # else :
        #     messages.success(request, "哈哈哈哈")
        # 渲染待添加页面给用户
    return render(request, 'login.html')

@csrf_exempt
def register(request):
    # POST请求表示用户已提交数据
    if request.method == 'POST':
        name = request.POST.get("in_name")
        email = request.POST.get("in_email")
        password = request.POST.get("in_password")
        re_password = request.POST.get("re_password")
        if name and email and password and(password==re_password):
            if models.user.objects.filter(name=name) or models.user.objects.filter(name=email):
                messages.info(request, "用户名或邮箱已存在！")
                return redirect('/register/')
            else:
                models.user.objects.create(name=name, password=password, email=email)
                return redirect('/login/')
    # 渲染待添加页面给用户
    return render(request, 'register.html')

@csrf_exempt
def admin(request):
    if request.session['D_name'] == "root":
        print(request.method)
        if request.method == 'POST':
            name =request.POST.get("D_name")
            password=request.POST.get("D_pwd")
            print(name,password)
            models.user.objects.filter(name =name).update(password=password)
            # users = models.user.objects.all()
            # files = models.library.objects.all()
            # return render(request, "admin.html", {'user_list': users,'file_list': files})
        users = models.user.objects.all()
        files = models.library.objects.all()
        return render(request,"admin.html",{'user_list': users,'file_list': files})
    return render(request, 'welcome.html')

@csrf_exempt
def upload(request):
    if request.session['D_name'] != '':
        if request.method == 'POST':
            title = models.library.objects.filter(name=request.POST['send_title'])
            form = UploadFileForm(request.POST, request.FILES)
            print(form)
            if form.is_valid() and not title:
                send_department = models.user.objects.get(name=request.session['D_name'])
                acc_department = models.user.objects.get(email=request.POST['send_email'])
                models.library.objects.create(name=request.POST['send_title'], path='upload\\'+request.POST['send_title'], pac_path='/static/image/assa.jpg', send_department=send_department, acc_department=acc_department, level="f_level")
                handle_uploaded_file(request.POST['send_title'], request.FILES['send_file'])
                print("OK!")
                return render(request, 'send.html')
            print("NO!")
        return render(request, 'send.html')
    return render(request, 'welcome.html')

@csrf_exempt
def download(request):
    if request.session['D_name'] != '':
        if request.method == 'POST':
            filename = request.POST.get('name')
            content = "attachment;filename=" + filename
            file = open('upload/'+filename, 'rb')
            response = HttpResponse(file)
            response['Content-Type'] = 'application/octet-stream'  # 设置头信息，告诉浏览器这是个文件
            response['Content-Disposition'] = content
            return response
        return render(request, 'index.html')
    return render(request, 'welcome.html')

@csrf_exempt
def delete(request):
    if request.session['D_name'] != '':
        if request.method == 'POST':
            filename = request.POST.get('name')
            file = models.library.objects.get(name=filename)
            file.is_delete = 1
            file.save()
            return render(request, 'receive.html')
        return render(request, 'receive.html')
    return render(request, 'welcome.html')

def receive(request):        #数据绑定
    if request.session['D_name'] != '':
        print(request.method)
        D_id = request.session.get('D_id')
        if D_id == 1:
            files = models.library.objects.all()
        else:
            files = models.library.objects.filter(acc_department=D_id, is_delete='')
        print(files)
        return render(request, 'receive.html', {'filelist': files})
    return render(request, 'welcome.html')

def destroy(request):
    if request.session['D_name'] != '':
        request.session['D_id']=''
        request.session['D_name']=''
        return render(request, 'welcome.html')
    return render(request, 'welcome.html')

def page_not_found(request,exception):
    context = "页面不存在、或已被删除、或暂时不可用"
    return render(request, 'error.html', {"err" : '404', "context" : context}, status=404)

def forbidden(request,exception):
    context = "403 Forbidden"
    return render(request, 'error.html', {"err" : '404', "context" : context}, status=403)

def error(request):
    context = "服务器发生错误"
    return render(request, 'error.html', {"err" : '500', "context" : context}, status=500)