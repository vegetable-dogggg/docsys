from django import forms

class UploadFileForm(forms.Form):
    send_email = forms.CharField(max_length=50)
    send_title = forms.CharField(max_length=50)
    send_file = forms.FileField()